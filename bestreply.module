<?php

/**
 * @file
 * This is the Actions module for executing stored actions.
 */

use Drupal\comment\CommentInterface;
use Drupal\comment\Plugin\Field\FieldType\CommentItemInterface;
use Drupal\Core\Entity\Display\EntityViewDisplayInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Link;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\Core\Url;
use Drupal\node\NodeInterface;

/**
 * Implements hook_help().
 */
function bestreply_help($route_name, RouteMatchInterface $route_match) {
  switch ($route_name) {
    case 'help.page.bestreply':
      $output = '';
      $output .= '<h3>' . t('About') . '</h3>';
      $output .= '<p>' . t('Best reply module allows node authors to mark a particular comment as the best reply. It aims to encourage quality comments and to give some recognition to people posting quality responses.') . '</p>';
      return $output;
  }
}

/**
 * Implements hook_ENTITY_TYPE_view().
 */
function bestreply_node_view(array &$build, EntityInterface $entity, EntityViewDisplayInterface $display, $view_mode) {
  if ($view_mode == 'full' || $view_mode == 'default') {
    $br_types = \Drupal::config('bestreply.settings')->get('types');
    $br_name = \Drupal::config('bestreply.settings')->get('name');
    if (!isset($br_types[$entity->bundle()]) || $br_types[$entity->bundle()] == '0') {
      return;
    }
    $br_id = bestreply_ismarked($entity->id());

    $build['#attached']['library'][] = 'bestreply/bestreply';
    $build['#attached']['drupalSettings']['bestreply']['name'] = $br_name;
    $build['#attached']['drupalSettings']['bestreply']['ismarked'] = $br_id;

  }
}

/**
 * Implements hook_node_links_alter().
 *
 * Adds the bestreply link after the 'Add new comment' and not before.
 */
function bestreply_node_links_alter(array &$links, NodeInterface $node, array &$context) {
  $view_mode = $context['view_mode'];
  if ($view_mode == 'full' || $view_mode == 'default') {
    $br_types = \Drupal::config('bestreply.settings')->get('types');
    if (!isset($br_types[$node->bundle()]) || $br_types[$node->bundle()] == '0') {
      return;
    }
    $br_id = bestreply_ismarked($node->id());
    $br_name = \Drupal::config('bestreply.settings')->get('name');
    $comment_manager = \Drupal::service('comment.manager');
    $fields = $comment_manager->getFields($node->getEntityTypeId());

    $if_comments_available = FALSE;
    foreach ($fields as $field_name => $detail) {
      if ($node->hasField($field_name) && $node->get($field_name)->status != CommentItemInterface::HIDDEN) {
        $if_comments_available = TRUE;
      }
    }

    if ($if_comments_available && $br_id && \Drupal::currentUser()->hasPermission('view bestreply')) {
      $bestreply_comment_link = t('View @bestreply', ['@bestreply' => $br_name]);
      $bestreply_links['bestreply-view'] = [
        'title' => $bestreply_comment_link,
        'url' => $node->toUrl('canonical', ['fragment' => 'comment-' . $br_id]),
        'language' => $node->language(),
        'attributes' => [
          'id' => 'bestreply-view',
          'class' => 'br_view',
          'title' => $bestreply_comment_link,
        ],
      ];
      $links['bestreply_view'] = [
        '#theme' => 'links__node__node',
        '#links' => $bestreply_links,
        '#attributes' => ['class' => ['links', 'inline']],
      ];
    }
  }
}

/**
 * Alter the links of a comment.
 *
 * Implementation of hook_comment_links_alter().
 */
function bestreply_comment_links_alter(array &$links, CommentInterface $comment, array &$context) {
  $best_reply_link = '';
  if ($context["view_mode"] == 'full' || $context["view_mode"] == 'default') {
    $account = \Drupal::currentUser();
    $entity = $comment->getCommentedEntity();
    $bundle = $entity->bundle();
    $br_types = \Drupal::config('bestreply.settings')->get('types');
    $br_name = \Drupal::config('bestreply.settings')->get('name');
    $br_change = \Drupal::config('bestreply.settings')->get('change');

    if (!isset($br_types[$bundle]) || $br_types[$bundle] == '0') {
      return;
    }
    $entity_uid = $entity->get('uid')->target_id;
    $author = ($account->id() == $entity_uid);
    $moderator = $account->hasPermission('moderate bestreply');
    $br_cid = bestreply_ismarked($entity->id());
    $best_reply_link = [];
    if (!$br_cid) {
      if (($author && \Drupal::currentUser()->hasPermission('mark bestreply')) || $moderator) {
        $best_reply_link['comment-bestreply'] = [
          'title' => $br_name,
          'url' => Url::fromRoute('bestreply.mark', ['comment' => $comment->id()]),
          'attributes' => [
            'title' => t('Mark this comment as the @bestreply.', ['@bestreply' => $br_name]),
            'class' => ['br_mark'],
          ],
        ];
      }
    }
    else {
      if ($br_cid == $comment->id()) {
        if (($author && $account->hasPermission('clear bestreply')) || $moderator) {
          $best_reply_link['comment-bestreply'] = [
            'title' => t('Clear @bestreply', ['@bestreply' => $br_name]),
            'url' => Url::fromRoute('bestreply.clear', ['comment' => $comment->id()]),
            'attributes' => [
              'title' => t('Clear this @bestreply.', ['@bestreply' => $br_name]),
              'class' => ['br_clear'],
            ],
          ];
        }
      }
      else {
        if (($author && $account->hasPermission('mark bestreply') && $br_change) || $moderator) {
          $best_reply_link['comment-bestreply'] = [
            'title' => $br_name,
            'url' => Url::fromRoute('bestreply.mark', ['comment' => $comment->id()]),
            'attributes' => [
              'title' => t('Set this comment as the @bestreply.', ['@bestreply' => $br_name]),
              'class' => ['br_mark'],
            ],
          ];
        }
      }
    }
  }
  if ($best_reply_link) {
    $links['bestreply'] = [
      '#theme' => 'links__comment__bestreply',
      '#attributes' => ['class' => ['links', 'inline']],
      '#links' => $best_reply_link,
    ];
  }
}

/**
 * Check if bestreply comment exist on this page.
 *
 * @see template_preprocess_comment
 */
function bestreply_preprocess_comment(&$variables) {
  $comment = $variables['elements']['#comment'];
  if (isset($comment->bestreply) && $comment->bestreply === 1) {
    if (isset($comment->in_preview)) {
      $variables['title'] = $comment->getSubject();
    }
    else {
      $uri = $comment->toUrl();
      $attributes = $uri->getOption('attributes') ?: [];
      $attributes += [
        'class' => ['permalink', 'bestreply'],
        'fragment' => 'comment-' . $comment->id(),
        'rel' => 'bookmark',
      ];
      $uri->setOption('attributes', $attributes);
      $variables['title'] = Link::fromTextAndUrl($comment->getSubject(), $uri)->toRenderable();
    }
    $variables['#attached']['drupalSettings']['bestreply']['onthispage'] = 1;
  }
}

/**
 * Implements hook_comment_load().
 */
function bestreply_comment_load($comments) {
  // The comment id of the best reply.
  $first_comment = current($comments);
  $c_marked = bestreply_ismarked($first_comment->id());
  // Can the user view.
  $user_can_view = \Drupal::currentUser()->hasPermission('view bestreply');
  foreach ($comments as $comment) {
    if ($comment->id() && $comment->id() == $c_marked && $user_can_view) {
      // Add new variable to the comment so we can easily theme.
      $comment->bestreply = 1;
    }
    else {
      $comment->bestreply = 0;
    }
  }
}

/**
 * Return the marked cid (comment id) for the given entity id.
 */
function bestreply_ismarked($e_id = NULL) {
  if (!$e_id) {
    return FALSE;
  }
  return \Drupal::service('database')
    ->query('SELECT cid FROM {bestreply} WHERE nid = :nid', [':nid' => $e_id])
    ->fetchField();
}

/**
 * Implements hook_ENTITY_TYPE_delete().
 */
function bestreply_comment_delete(EntityInterface $comment) {
  $e_id = $comment->getCommentedEntityId();
  $br_id = bestreply_ismarked($e_id);
  // If best reply ID is the same as cid, then clear the best reply.
  if ($br_id === $comment->id()) {
    \Drupal::service('bestreply')->clear($comment);
  }
}
