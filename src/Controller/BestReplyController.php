<?php

namespace Drupal\bestreply\Controller;

use Drupal\comment\CommentInterface;
use Drupal\Component\Datetime\Time;
use Drupal\Component\Serialization\Json;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Cache\Cache;
use Drupal\Core\Config\ConfigFactory;
use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DateFormatter;
use Drupal\Core\DependencyInjection\ContainerInjectionInterface;
use Drupal\Core\Link;
use Drupal\Core\Session\AccountProxyInterface;
use Drupal\Core\Url;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Controller routines for bestreply routes.
 */
class BestReplyController extends ControllerBase implements ContainerInjectionInterface {

  /**
   * The database connection.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $connection;

  /**
   * A current user instance.
   *
   * @var \Drupal\Core\Session\AccountProxyInterface
   */
  protected $currentUser;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  private $requestStack;

  /**
   * Configuration.
   *
   * @var \Drupal\Core\Config\ConfigFactory
   */
  private $config;

  /**
   * Time.
   *
   * @var \Drupal\Component\Datetime\Time
   */
  protected $time;

  /**
   * Date formatter.
   *
   * @var \Drupal\Core\Datetime\DateFormatter
   */
  protected $dateFormatter;

  /**
   * Construct.
   *
   * @param \Drupal\Core\Database\Connection $connection
   *   The database connection.
   * @param \Drupal\Core\Session\AccountProxyInterface $current_user
   *   The current user account.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Config\ConfigFactory $config_factory
   *   The config factory.
   * @param \Drupal\Component\Datetime\Time $datetime_time
   *   Time.
   * @param \Drupal\Core\Datetime\DateFormatter $date_formatter
   *   The date formatter.
   */
  public function __construct(
    Connection $connection,
    AccountProxyInterface $current_user,
    RequestStack $request_stack,
    ConfigFactory $config_factory,
    Time $datetime_time,
    DateFormatter $date_formatter
  ) {
    $this->connection = $connection;
    $this->currentUser = $current_user;
    $this->requestStack = $request_stack;
    $this->config = $config_factory;
    $this->time = $datetime_time;
    $this->dateFormatter = $date_formatter;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('database'),
      $container->get('current_user'),
      $container->get('request_stack'),
      $container->get('config.factory'),
      $container->get('datetime.time'),
      $container->get('date.formatter')
    );
  }

  /**
   * Insert or update the marked comment info.
   */
  public function mark(CommentInterface $comment, $js = NULL) {
    $user = $this->currentUser;
    $rt = FALSE;
    $dt = $this->time->getRequestTime();

    if ($comment->isPublished()) {
      if (bestreply_ismarked($comment->getCommentedEntityId())) {
        $action = 'replace';
        $rt = $this->connection->query('UPDATE {bestreply} SET cid = :cid, aid = :aid, uid = :uid, dt = :dt  where nid = :nid',
            [
              'cid' => $comment->id(),
              'aid' => $comment->getOwnerId(),
              'uid' => $user->id(),
              'dt' => $dt,
              'nid' => $comment->getCommentedEntityId(),
            ]);
      }
      else {
        $action = 'mark';
        $rt = $this->connection->query('INSERT into {bestreply} values( :nid, :cid, :aid, :uid, :dt)',
            [
              'nid' => $comment->getCommentedEntityId(),
              'cid' => $comment->id(),
              'aid' => $comment->getOwnerId(),
              'uid' => $user->id(),
              'dt' => $dt,
            ]);
      }

      Cache::invalidateTags(['node:' . $comment->getCommentedEntityId()]);

      if ($js) {
        $status = ($rt) ? TRUE : FALSE;
        print Json::encode([
          'status' => $status,
          'cid' => $comment->id(),
          'action' => $action,
        ]);
        exit;
      }
    }
  }

  /**
   * Clear the marked comment info.
   */
  public function clear(CommentInterface $comment, $js = NULL) {
    if (bestreply_ismarked($comment->getCommentedEntityId())) {
      $rt = $this->connection->query("DELETE FROM {bestreply} WHERE nid = :nid", ['nid' => $comment->getCommentedEntityId()]);
      Cache::invalidateTags(['node:' . $comment->getCommentedEntityId()]);
    }
    if ($js) {
      $status = ($rt) ? TRUE : FALSE;
      print Json::encode([
        'status' => $status,
        'cid' => $comment->id(),
        'action' => 'clear',
      ]);
      exit;
    }
  }

  /**
   * Checks if current user is comment entity author or has the moderator role.
   */
  public function checkAuthorOrModerator(CommentInterface $comment) {
    $account = $this->currentUser;
    $entity = $comment->getCommentedEntity();
    $entity_uid = $entity->get('uid')->target_id;
    $author = ($account->id() == $entity_uid);
    $moderator = $account->hasPermission('moderate bestreply');

    return AccessResult::allowedIf($author || $moderator);
  }

  /**
   * List all the best reply data.
   */
  public function replyCommentList() {
    $head = [
    ['data' => 'title'],
    ['data' => 'comment', 'field' => 'subject'],
    ['data' => 'author', 'field' => 'aid', 'sort' => 'asc'],
    ['data' => 'marked by', 'field' => 'name', 'sort' => 'asc'],
    ['data' => 'when', 'field' => 'dt', 'sort' => 'asc'],
    ];

    $sql = $this->connection->select('bestreply', 'b')
      ->fields('b', ['nid', 'cid', 'uid', 'aid', 'dt']);

    $sql->join('node_field_data', 'n', 'n.nid = b.nid');
    $sql->addField('n', 'title');
    $sql->join('comment_field_data', 'c', 'c.cid = b.cid');
    $sql->addField('c', 'subject');
    $sql->join('users_field_data', 'u', 'u.uid = b.uid');
    $sql->addField('u', 'name');

    $sql->extend('Drupal\Core\Database\Query\PagerSelectExtender')
      ->extend('Drupal\Core\Database\Query\TableSortExtender')
      ->orderByHeader($head);

    $result = $sql->execute()->fetchAll();
    $anonymous = $this->config('user.settings')->get('anonymous');
    foreach ($result as $reply) {
      $options = ['fragment' => 'comment-' . $reply->cid];
      $comment_author_name = $reply->name;
      $author = !empty($reply->aid) ? Link::fromTextAndUrl($comment_author_name, Url::fromUri('entity:user/' . $reply->aid)) : $anonymous;
      $reply_user = !empty($reply->uid) ? Link::fromTextAndUrl($reply->name, Url::fromUri('entity:user/' . $reply->uid)) : $anonymous;
      $rows[] = [
        Link::fromTextAndUrl($reply->title, Url::fromUri('entity:node/' . $reply->nid, $options)),
        $reply->subject,
        $author,
        $reply_user,
        $this->t('@time ago', ['@time' => $this->dateFormatter->formatInterval($this->time->getRequestTime() - $reply->dt)]),
      ];
    }

    if (isset($rows)) {
      // Add the pager.
      $build['content'] = [
        '#theme' => 'table',
        '#header' => $head,
        '#rows' => $rows,
      ];
      $build['pager'] = [
        '#type' => 'pager',
      ];
      return $build;
    }
    else {
      return [
        '#markup' => $this->t('No results to display'),
      ];
    }
  }

}
